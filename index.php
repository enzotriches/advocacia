<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="UTF-8">
	<title>Advocacia</title>
	<link rel="stylesheet" href="Assets/css/style.css">
	<link rel="stylesheet" href="Assets/css/bootstrap.min.css">
  
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">

  <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		
	<script src="Assets/js/pace.min.js"></script>
	<script src="Assets/js/jquery-3.2.1.min.js"></script>
	<script src="Assets/js/bootstrap.min.js"></script>
	<script src="Assets/js/animatescroll.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#home').click(function(){
				$('#inicio').animatescroll({scrollSpeed:2000,easing:'easeOutBack'});
			});
			$('#sobre').click(function(){
				$('#about').animatescroll({scrollSpeed:2000,easing:'easeOutBack'});
			});
			$('#areas').click(function(){
				$('#areas_options').animatescroll({scrollSpeed:2000,easing:'easeOutBack'});
			});
			$('#contato').click(function(){
				$('#contact').animatescroll({scrollSpeed:2000,easing:'easeOutBack'});
			});
			$('#top').click(function(){
				$('#inicio').animatescroll({scrollSpeed:2000,easing:'easeOutBack'});
			});


		});
	</script>
</head>
<body>
	<!-- H E A D E R  -->
	<nav class="navbar navbar-fixed-top navbar-expand-lg bg-light" id="navbar">
		<div class="container" id="inicio">
				<a class="navbar-brand a-logo" href="#">
					<img src="Assets/images/nav.png" width="32" height="32" class="d-inline-block align-top " alt="">
		   			<span class="h3 text-dark b">Advocacia</span>
		   			<small class="text-secondary">
		   				OAB - 92393-3313
		   			</small>
		   		</a>
				
				<button class="navbar-toggler bg-dark" type="button" data-toggle="collapse" data-target="#navbarSite">
					<span class="navbar-toggler-icon justify-content-center h-auto w-auto text-light b ">
						Menu
					</span>
				</button>
			
				<div class="collapse navbar-collapse justify-content-md-center" id="navbarSite">
					
					<ul class="navbar-nav ml-auto">
						<ul class="navbar-nav mr-1 mt-2 ">
							<li class="nav-item dropdown mr-lg-3 ml-1 b">
								<a href="#" class="span text-dark b" id="home"><B>Início</B></a>
							</li>

							<li class="nav-item dropdown mr-lg-3 ml-1">
								<a href="#" class="span text-dark " id="sobre">Sobre</a>
							</li>

							<li class="nav-item dropdown mr-lg-3 ml-1 ">
								<a href="#" class="span text-dark " id="areas">Áreas de atuação</a>
							</li>						</ul>
						<li class="nav-item dropdown">
							<button class="btn bg-dark text-light " id="contato">Contato</button>
						</li>
					</ul>

				</div>
		</div>
	</nav>
	<!-- S L I D E S H O W -->
	<div id="slideshow" class="carousel slide" data-ride="carousel">
	  <ol class="carousel-indicators">
	    <li data-target="#slideshow" data-slide-to="0" class="active"></li>
	    <li data-target="#slideshow" data-slide-to="1"></li>
	    <li data-target="#slideshow" data-slide-to="2"></li>
	  </ol>
	  <div class="carousel-inner justify-content-center">
	    <div class="carousel-item active">
	      <img class="d-block w-100" src="Assets/images/image.jpg" alt="First slide">
	      <div class="carousel-caption">
	        <h1 class="text-center ">Lorem Ipsum</h1>
	        <p class="text-center">Dolor sit amet, sudo apt dotr</p>
	      </div>
	    </div>
	    <div class="carousel-item">
	      
	      <img class="d-block w-100" src="Assets/images/image1.jpg" alt="Second slide">
	       <div class="carousel-caption">
	        <h1 class="text-center ">Lorem Ipsum</h1>
	        <p class="text-center">Dolor sit amet, sudo apt dotr</p>
	      </div>
	    </div>

	    </div>
	    <div class="carousel-item">
	      <img class="d-block w-100" src="Assets/images/image2.jpg" alt="Third slide">
	       <div class="carousel-caption">
	        <h1 class="text-center ">Lorem Ipsum</h1>
	        <p class="text-center">Dolor sit amet, sudo apt dotr</p>
	      </div>
	    </div>
	    </div>
	  </div>
	  <a class="carousel-control-prev" href="#slideshow" role="button" data-slide="prev">
	    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
	    <span class="sr-only">Anterior</span>
	  </a>
	  <a class="carousel-control-next" href="#slideshow" role="button" data-slide="next">
	    <span class="carousel-control-next-icon" aria-hidden="true"></span>
	    <span class="sr-only">Próximo</span>
	  </a>
	</div>
	<!-- S O B R E -->
	<div class="container-fluid bg-light sobre pt-5 pb-5" id="about">
		<div class="row justify-content-center text-center pt" id="Sobre">
			<div class="col-12">
				<span class="h1 text-dark pt-5 pb-5">
					Sobre
				</span>
			</div>
			<div class="col-12 d-none d-lg-block col-lg-4">
				<div class="col-12 sobre-img">
					<img src="Assets/images/sobre.jpg" alt="Imagem sobre" class="w-100 rounded">
				</div>
			</div>
			<div class="col-12 col-lg-8 pb-1 pt-1 pr-lg-2">
				<div class="container">
					<blockquote class="blockquote text-start justify-content-end">
					    <p class="mb-0 font-italic texto-sobre">
					  			Lorem ipsum dolor sit amet, consectetur adipiscing elit. In semper dictum diam, pellentesque ultrices ligula porttitor vitae. 
					  		Nam varius nulla lorem, at malesuada turpis euismod id. Aliquam mollis malesuada nunc, at auctor lorem venenatis vel. 
					  		Proin hendrerit pharetra dolor, feugiat tincidunt elit cursus eu. Pellentesque ante diam, pretium ut tristique a, fermentum quis arcu. 
					  		In nec ex dictum, dictum mauris sagittis, laoreet diam. Duis congue diam leo, vitae aliquet erat lobortis eu. 
					  		Duis suscipit vestibulum lorem eget venenatis. Sed id lorem eu nisi lobortis luctus sed id massa.
					  		Aenean non sem finibus, malesuada turpis in, porta nisi. Aenean non iaculis tellus. 
					  		Praesent ornare enim vitae mi ullamcorper tempus eget at velit
						</p>
					  <footer class="blockquote-footer text-right">Advogado em <cite title="Source Title">João Pedro Silva</cite></footer>
					</blockquote>
				</div>
			</div>
		</div>
	</div>
	<!-- A R E A S  D E  A T U A Ç Ã O -->
	<div class="container-fluid areas text-light justify-content-center" id="areas_options">
		<div class="row">
			<div class="col-12 justify-content-center text-center">
				<h1>
					Áreas de atuação
				</h1>
			</div>
		</div>
		<div class="row justify-content-center text-center pt-1 pb-1">
			
			<div class="col-12 col-lg-4 rounded border-areas mb-5 mt-5 ">
				<div class="col-12">
					<h2 class="areas-op text-light">
						Civil
					</h2>
				</div>
				<div class="col-12 p-1">
					<button class="btn btn-lg btn-light" data-toggle="modal" data-target="#modal-civil">

						<i class="fa fa-info-circle text-purple"></i> Ações que possam me ajudar
					</button>
				</div>
			</div>
			<div class="col-12 col-lg-4 rounded border-areas mb-5 mt-5 ">
				<div class="col-12">
					<h2 class="areas-op text-light">
						Família
					</h2>
				</div>
				<div class="col-12 p-1">
					<button class="btn btn-lg btn-light"  data-toggle="modal" data-target="#modal-familia">
						<i class="fa fa-info-circle text-purple"></i> Ações que possam me ajudar
					</button>
				</div>
			</div>
			<div class="col-12 col-lg-4 rounded border-areas mb-5 mt-5 ">
				<div class="col-12">
					<h2 class="areas-op text-light">
						Trabalhista
					</h2>
				</div>
				<div class="col-12 p-1">
					<button class="btn btn-lg btn-light"  data-toggle="modal" data-target="#modal-trabalhista">
						<i class="fa fa-info-circle text-purple"></i> Ações que possam me ajudar
					</button>
				</div>
			</div>
			<div class="col-12 col-lg-4 rounded border-areas mb-5 mt-5 ">
				<div class="col-12">
					<h2 class="areas-op text-light">
						Penal
					</h2>
				</div>
				<div class="col-12 p-1">
					<button class="btn btn-lg btn-light"  data-toggle="modal" data-target="#modal-penal">
						<i class="fa fa-info-circle text-purple"></i> Ações que possam me ajudar
					</button>
				</div>
			</div>
			<div class="col-12 col-lg-4 rounded border-areas mb-5 mt-5 ">
				<div class="col-12">
					<h2 class="areas-op text-light">
						Empresarial
					</h2>
				</div>
				<div class="col-12 p-1">
					<button class="btn btn-lg btn-light"  data-toggle="modal" data-target="#modal-empresarial">
						<i class="fa fa-info-circle text-purple"></i> Ações que possam me ajudar
					</button>
				</div>
			</div>
			<div class="col-12 col-lg-4 rounded border-areas mb-5 mt-5 ">
				<div class="col-12">
					<h2 class="areas-op text-light">
						Previdenciário
					</h2>
				</div>
				<div class="col-12 p-1">
					<button class="btn btn-lg btn-light"  data-toggle="modal" data-target="#modal-previdenciario">
						<i class="fa fa-info-circle text-purple"></i> Ações que possam me ajudar
					</button>
				</div>
			</div>

		</div>
	</div>
	<!-- C O N T A T O S -->
	<div class="container-fluid contato justify-content-center" id="contact">
		<div class="row">
			<div class="col-12 justify-content-center text-center pb-4 p-2">
				<h1 class="text-dark">
					Contato
				</h1>
			</div>
			<div class="col-12 col-lg-8 localization pt-5">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d888426.7624671757!2d-51.03287675428728!3d-29.56246682931622!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x952208dc7014ad13%3A0xb3fe5782eb4e81e2!2sEscrit%C3%B3rio+de+Advocacia+Nelson+B.+Francisco!5e0!3m2!1spt-BR!2sbr!4v1551381201531" style=" width: 100%; min-height: 500px; border: none;" class="pb-5" allowfullscreen></iframe>
			</div>
			<div class="col-12 col-lg-4 localization-text p-2">
				<div class="col-12 justify-content-center">
					<h3 class="text-dark text-center">
						Nos envie um email!
					</h3>
				

					<form action="#" method="POST" class=" bg-light">
						<div class="form-control">
							<label for="">
								Nome completo: *
							</label>
							<input type="text" class="form-control" name="nome" placeholder="Nome completo">
						</div>
						<div class="form-control">
							<label for="">
								Assunto: *
							</label>
							<input type="text" class="form-control" name="assunto" placeholder="Nome completo">
						</div>
						<div class="form-control">
							<label for="">
								O que deseja nos enviar? *
							</label>
							<textarea name="mensagem" id="msg" cols="30" rows="5" class="form-control" placeholder="Mensagem..."></textarea>
						</div>
						<div class="form-control">
							<button class="btn btn-dark">Enviar</button>
						</div>
					</form>
				</div>
			</div>
			<div class="col-12 justify-content-center pt-3 pb-5 text-center">
				<div class="row text-center">
					<div class="col-12">
					<h2 class="text-dark">
						Redes sociais
					</h2>
					</div>
				</div>
				<div class="row justify-content-center text-center">
					 <div class="col-12 col-lg-4 pt-2">
					 	<div class="col-12">
					 		<i class="fa fa-instagram fa-7x"></i>
					 	</div>
					 	<div class="col-12">
					 		<span class="text text-center text-dark">
					 			Nos siga no Instagram!
					 		</span>
					 	</div>
					 	<div class="col-12">
						 	<button class="btn btn-light border">
						 		Acessar!
						 	</button>
					 	</div>
					 </div>
					  <div class="col-12 col-lg-4 pt-2">
					 	<div class="col-12">
					 		<i class="fab fa-whatsapp fa-7x"></i>
					 	</div>
					 	<div class="col-12">
					 		<span class="text text-center text-dark">
					 			Nos mande uma mensagem no WhatsApp!
					 		</span>
					 	</div>
					 	<div class="col-12">
					 		<span>(48) 9 9662-6060</span>
						 	<button class="btn btn-light border">
						 		<i class="far fa-copy"></i> Copiar
						 	</button>
					 	</div>
					 </div>
					  <div class="col-12 col-lg-4 pt-2">
					 	<div class="col-12">
					 		<i class="fa fa-facebook fa-7x"></i>
					 	</div>
					 	<div class="col-12">
					 		<span class="text text-center text-dark">
					 			Nos acompanhe no Facebook!
					 		</span>
					 	</div>
					 	<div class="col-12">
						 	<button class="btn btn-light border">
						 		Abrir!
						 	</button>
					 	</div>
					 </div>	
				</div>
			</div>
		
		</div>
	</div>

	<!-- F O O T E R -->
	<footer class="footer container-fluid bg-dark pt-2 pb-5 text-light">
		<div class="row">	
				<div class="col-8">	
					<span class="text justify-content-start text-start pl-5">
						Site desenvolvido por <a href="#" class="text-light"><b><u>	Enzo Trichês </u></b></a>. <br>
					</span>
				</div>
				<div class="col-4 justify-content-end text-right">
					<button class="btn btn-light border text-center justify-content-center" id="top">
						<i class="fas fa-sort-up text-dark"></i>
					</button>
				</div>
		</div>
	</footer>

<!-- Modal -->




	<!-- CIVIL -->
	<div class="modal fade" id="modal-civil" tabindex="-1" role="dialog" aria-labelledby="modal-civil" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-scrollable" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modal-civil">Direito civil</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<h3 class="text-dark text-center">Alguns casos de atuação:</h3>
	      	<ul>
	        <li>Disputa de herança</li>
			<li> Processo de divórcio</li>
			<li> Indenização, danos morais e físicos</li>
			<li> Inventário e partilha de bens</li>
			<li> Negociação e renegociação de contratos</li>
			<li> Cobrança e despejo</li>
			<li> Adoção e tutela</li>
			<li> Compra, venda e locação de imóveis</li>
			<li> Testamento</li>
			<li> Acordo pré-nupcial</li>
			<li> Contratos de compra e venda de bens</li>
			<li> Recuperação de crédito e quitação de débitos</li>
			</ul>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar janela</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- FAMÍLIA -->
	<div class="modal fade" id="modal-familia" tabindex="-1"  role="dialog" aria-labelledby="modal-familia" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-scrollable" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modal-familia">Direito Família</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<h3 class="text-dark text-center">Alguns casos de atuação:</h3>
	      	<ul>
		        <li>Casamento
		            <ul>
		                <li>regime de bens</li>
		                <li>aspectos patrimoniais</li>
		                <li>direitos e deveres dos cônjuges</li>
		                <li>acordo pré-nupcial</li>
		                <li>anulação de casamento</li>
		            </ul>
		        </li>
		        <li>Separação e Divórcio
		            <ul>
		                <li>separação consensual judicial</li>
		                <li>separação consensual cartório</li>
		                <li>ações de separação litigiosos</li>
		                <li>ações de divórcio consensuais</li>
		                <li>ações de divórcio litigiosos </li>
		                <li>dissolução do matrimônio</li>
		                <li>guarda dos filhos</li>
		                <li>regulamentação de visitas</li>
		                <li>pensão alimentícia para o cônjuge</li>
		                <li>pensão alimentícia para os filhos</li>
		                <li>partilha do patrimônio do casal</li>
		                <li>alimentos e execução de alimentos</li>
		            </ul>
		        </li>
		        <li>inventários</li>
		        <li>testamento</li>
		        <li>guarda judicial</li>
		        <li>direito de visitas</li>
		        <li>tutela</li>
		        <li>adoção</li>
		        <li>sucessões</li>
		        <li>interdição</li>
		        <li>investigação de paternidade</li>
		        <li>separação de corpos</li>
		        <li>direito a fertilização invitro pelo plano de saúde</li>
			</ul>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar janela</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- TRABALHISTA -->
	<div class="modal fade" id="modal-trabalhista" tabindex="-1"  role="dialog" aria-labelledby="modal-trabalhista" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-scrollable" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modal-trabalhista">Direito Trabalhista</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<h3 class="text-dark text-center">Alguns casos de atuação:</h3>
	      	<ul>
		      	<li>Férias</li>
				<li>13º salário</li>
				<li>Horas extras</li>
				<li>Jornadas abusivas de trabalho</li>
				<li>Terceirizações</li>
				<li>Dissídios coletivos</li>
				<li>Aviso prévio</li>
				<li>FGTS e INSS</li>
				<li>Justa causa</li>
				<li>Rescisão contratual</li>
				<li>Seguro desemprego</li>
				<li>Acidentes e doenças no trabalho</li>
				<li>Assédio físico e moral</li>
				<li>Trabalhador sem vínculo (carteira assinada)</li>
				<li>Licença maternidade</li>
				<li>Vale transporte</li>
				<li>Dispensa coletiva</li>
				<li>Insalubridade e periculosidade no ambiente de trabalho</li>
			</ul>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar janela</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Penal -->
	<div class="modal fade" id="modal-penal" tabindex="-1"  role="dialog" aria-labelledby="modal-penal" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-scrollable" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modal-penal">Direito Penal</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<h3 class="text-dark text-center">Alguns casos de atuação:</h3>
	      	<ul>
		      	<li> Queixa criminal</li>
				<li> Liberdade provisória</li>
				<li> Assalto e furto</li>
				<li> Homicídio ou tentativa</li>
				<li> Recurso</li>
				<li> Inquérito policial</li>
				<li> Sonegação de imposto</li>
				<li> Lavagem de dinheiro</li>
				<li> Infração das leis ambientais</li>
				<li> Procedimentos ilícitos na área tributária</li>
				<li> Estelionato</li>
				<li> Falsidade ideológica</li>
				<li> Tráfico e contrabando</li>
				<li> Violência</li>
				<li> Abuso e exploração sexual</li>
				<li> Corrupção de menor</li>
				<li> Omissão de socorro</li>
				<li> Dirigir embriagado</li>
				<li> Lesão corporal</li>
				<li> Apropriação indébita</li>
				<li> Trabalho escravo</li>
			</ul>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar janela</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Empresarial -->
	<div class="modal fade" id="modal-empresarial" tabindex="-1"  role="dialog" aria-labelledby="modal-empresarial" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-scrollable" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modal-empresarial">Direito Empresarial</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<h3 class="text-dark text-center">Alguns casos de atuação:</h3>
		    <ul>
		    	<li> Contratos de empréstimos junto a bancos</li>
				<li> Fusões, incorporações e parcerias entre empresas</li>
				<li> Registro de pessoa jurídica (CNPJ)</li>
				<li> Abertura e fechamento de empresa</li>
				<li> Compra, venda e permutas</li>
				<li> Estatutos sociais</li>
				<li> Acordos entre acionistas</li>
				<li> Investigação de oportunidade de negócio</li>
				<li> Proteção da marca</li>
				<li> Análise de campanhas publicitárias antes de irem ao ar</li>
				<li> Reestruturação ou dissolução de sociedades</li>
				<li> Planejamento tributário</li>
				<li> Análise de concorrência</li>
				<li> Falência</li>
				<li> Recuperação judicial</li>
				<li> Proteção do patrimônio material, intelectual e moral</li>
				<li> Prestação de contas</li>
				<li> Exclusão da responsabilidade dos sócios em ações</li>
		    </ul>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar janela</button>
	      </div>
	    </div>
	  </div>
	</div>

	<!-- Previdenciário -->
	<div class="modal fade" id="modal-previdenciario" tabindex="-1"  role="dialog" aria-labelledby="modal-previdenciario" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-scrollable" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modal-previdenciario">Direito Previdenciário</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	      	<h3 class="text-dark text-center">Alguns casos de atuação:</h3>
		    <ul>
		    	<li> Aposentadoria por idade</li>
				<li> Aposentadoria por invalidez</li>
				<li> Aposentadoria por tempo de contribuição</li>
				<li> Aposentadoria proporcional</li>
				<li> Aposentadoria de trabalhador rural</li>
				<li> Aposentadoria de servidor público</li>
				<li> Contagem de tempo de contribuição</li>
				<li> Desaposentação</li>
				<li> Auxílio-doença</li>
				<li> Auxílio-acidentário</li>
				<li> Pensão por morte</li>
				<li> Revisão de aposentadoria, pensão ou auxílio</li>
				<li> Salário maternidade</li>
				<li> Recolhimento de valores INSS</li>
				<li> Recuperação de contribuições recolhidas indevidamente</li>
		    </ul>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Fechar janela</button>
	      </div>
	    </div>
	  </div>
	</div>
		


</body>
</html>